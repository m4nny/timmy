BITS 16
ORG 0x7c00


%define MBR_MAGIC           0xaa55
%define MINIX1_30CHARS      0x138f
%define MINIX1_VALID        0x01

%define chunk_tmp_addr      0x500
%define indirect_zones_addr 0x700
%define memprobe_addr       0xb00

%define inodetable_addr     0x8000
%define bootdir_location    0x2000
%define rootdir_location    inodetable_addr

%define kernel_addr         0x100000
%define hdd_stub            0x8000


; noreturn, trash everything
start:
    jmp     0x0:entry


ALIGN 4
gdt: ; waste only 1 byte (jmp is 3 bytes)
    dq      0
    dw      0xFFFF, 0, 0x9200, 0x00CF ; data
    dw      0xFFFF, 0, 0x9A00, 0x00CF ; code
gdt_info:
    dw      gdt_info - gdt - 1
    dd      gdt
disk_specs:
    dw      0x0 ; dl=disk id
    dw      0x0 ; sector count
    dw      0x0 ; head count
    dw      0x0 ; drive count


; both the boot and kernel names are intentionaly short since it
; is pretty hard to fit the bootloader into the tiny blank block.
; there is the same problem with the error messages, which, when
; very descriptive take a surprisingly big amount of space.
; therefore, when ecountering an error, one should read the
; bootloader code in order to understand the problem origin.
boot_folder_name:
    db      "boot", 0
kernel_name:
    db      "kernel", 0
hdd_stub_name:
    db      "hddstub", 0
perror_msg:
    db      "error: ", 0
diskinit_error_msg:
    db      "init", 0
read_sectors_error_msg:
    db      "read", 0
load_file_error_msg:
    db      "load", 0
load_superblock_error_msg:
    db      "fs", 0
parse_directory_not_found_error:
    db      "file", 0
memprobe_error_msg:
    db      "memory", 0


; noreturn, trash everything
entry:
    ; init everything
    mov     esp, 0x7c00
    call    initialization

    ; enable a20 line and switch to unreal mode
    call    unreal_mode

    ; check various stuff, retrieve disk geometry...
    call    diskinit

    ; load next part of bootloader
    call    load_ext_bootloader

    ; get into the next part of the bootloader
    jmp     load_kernel


; dx=drive number
; trash everything
; returns 0 in every register and segment register
initialization:
    mov     WORD [disk_specs], dx
    xor     edx, edx
    xor     eax, eax
    xor     ebx, ebx
    xor     ecx, ecx
    xor     esi, esi
    xor     edi, edi
    ret


diskinit:
    pushad
    ; setup args
    xor     edi, edi
    mov     ah, 0x8
    mov     dx, WORD [disk_specs]
    int     0x13

    ; check for errors
    jc      diskinit_error
    cmp     ah, 0
    jne     diskinit_error
    cmp     dl, 0
    je      diskinit_error
    cmp     cx, 0
    je      diskinit_error

    ; store geometry
    and     cl, 0x3f
    mov     BYTE [disk_specs + 0x2], cl ; set 5 bits of sector count
    mov     BYTE [disk_specs + 0x4], dh ; store head count
    add     WORD [disk_specs + 0x4], 1 ; reported headc is real headc-1
    mov     BYTE [disk_specs + 0x6], dl ; store

    popad
    ret
    diskinit_error:
    mov     edi, diskinit_error_msg
    call    perror


; takes no argument
; trash nothing
; returns nothing
unreal_mode:
    pushad

    ; disable interrupts, load gdt infos, save %es base address
    push    es
    cli
    lgdt    [gdt_info]

    ; switch to pmode
    mov     eax, cr0
    or      al, 1
    mov     cr0, eax

    ; load the segment selector into %es
    mov     bx, 0x8
    mov     es, bx

    ; switch from pmode
    mov     eax, cr0
    and     al, 0xfe
    mov     cr0, eax

    ; restore flags, es base address, and leave
    pop     es
    popad
    ret


; takes no argument
; trash ax, bx, cx
; return nothing
load_ext_bootloader:
    mov     ax, 1
    mov     bx, 1
    mov     edi, 0x7e00
    call    load_sectors
    ret


; ax=sector count, bx=lba offset, edi=memory offset
; trash nothing
; returns nothing
load_sectors:
    pushad
    add     ax, bx

    load_sectors_copy_loop:
    ; retrieve the chunk from the disk
    push    ax
    mov     al, 1
    call    read_sectors
    pop     ax

    ; copy that chunk at index %edi
    mov     cx, 0x200
    mov     esi, chunk_tmp_addr
    a32     rep movsb

    inc     bx
    cmp     bx, ax
    jne     load_sectors_copy_loop

    popad
    ret


; al=nbr of sectors to read, bx=lba offset
; trash nothing
; returns nothing
read_sectors:
    pushad
    mov     ah, 0x2
    push    WORD chunk_tmp_addr
    push    ax
    mov     bp, sp

    xor     dx, dx
    mov     ax, bx
    idiv    WORD [disk_specs + 0x2]
    push    ax ; temporary var

    xor     dx, dx
    mov     ax, bx
    idiv    WORD [disk_specs + 0x2]
    inc     dx
    push    dx ; sector number

    xor     dx, dx
    mov     ax, WORD [bp - 2]
    idiv    WORD [disk_specs + 0x4]
    push    dx ; head number

    xor     dx, dx
    mov     ax, WORD [bp - 2]
    idiv    WORD [disk_specs + 0x4]
    push    ax ; cylinder number

    ; low eight bits of cyl number
    mov     ch, BYTE [bp - 8]

    ; sector number and high 2 bits cyl number
    mov     al, BYTE [bp - 7]
    shl     al, 6
    mov     cl, BYTE [bp - 4]
    or      cl, al

    ; head number and drive number
    mov     dh, BYTE [bp - 6]
    mov     dl, BYTE [disk_specs]

    add     sp, 8
    pop     ax
    pop     bx
    int     0x13
    jc      read_sectors_error

    popad
    ret
    read_sectors_error:
    mov     edi, read_sectors_error_msg
    call    perror


; esi=str1, bx=str2
; trash nothing
; clear zero flag if equals, set if not
strcmp:
    pushad
    mov     bp, bx

    strcmp_loop:
    mov     al, BYTE [esi]
    cmp     al, BYTE [bp]
    jne     strcmp_end ; zero flag clear
    inc     esi
    inc     bp

    cmp     al, 0
    jne     strcmp_loop
    ; zero flag set

    strcmp_end:
    popad
    ret


; si=msg
; trash nothing
; returns nothing
putstr:
    pushad
    cld

    putstr_loop:
    lodsb
    cmp     al, 0x0
    je      putstr_ok

    mov     bx, 0x1
    mov     ah, 0xe
    int     0x10
    jmp     putstr_loop

    putstr_ok:
    popad
    ret


; edi=string addr
; trash nothing
; doesn't return
perror:
    mov     esi, perror_msg
    call    putstr
    mov     esi, edi
    call    putstr
    ; we end up in halt


; takes no argument
; trash nothing
; doesn't return
halt:
    hlt
    jmp     halt


; takes no argument
; trash eax, esp
; noreturn
jump_kernel:
    ; disable interrupts and load gdt
    cli
    lgdt    [gdt_info]

    ; actual switch to protected mode and leave the routine
    mov     eax, cr0
    or      al, 1
    mov     cr0, eax

    mov     ax, 0x8
    mov     ds, ax
    mov     ss, ax

    ; set the CS register
    jmp     0x10:pmode

    BITS 32
    pmode:
    jmp     kernel_addr
    BITS 16


magic:
    times   510-($-$$) db 0x00
    dw      MBR_MAGIC


load_kernel:
    ; probe whole memory and save resulting linked list
    ; for use by the kernel later on
    mov     edi, memprobe_addr
    call    memprobe

    ; load superblock and check minix fs
    mov     edi, 0x0c00
    call    load_superblock

    ; load inode table
    mov     edi, inodetable_addr
    mov     esi, 0x0c00
    call    load_itable

    ; load root directory (first inode in itable)
    mov     esi, rootdir_location
    mov     edi, 0x1000
    call    load_file

    ; get the inode address of the boot directory
    mov     esi, 0x1000
    mov     edi, inodetable_addr
    mov     bx, boot_folder_name
    call    parse_directory

    ; load the boot directory itself
    mov     esi, edi
    mov     edi, bootdir_location
    call    load_file

    ; get the inode address of the kernel image
    mov     esi, bootdir_location
    mov     edi, inodetable_addr
    mov     bx, kernel_name
    call    parse_directory

    ; load the kernel image itself
    mov     esi, edi
    mov     edi, kernel_addr
    call    load_file

    ; bootstrapping is done, jump into the kernel now
    ; setup arguments to kernel
    push    DWORD memprobe_addr ; memory detection list
    jmp     jump_kernel


; edi=dst
; trash ax, bx, edi
; return nothing
load_superblock:
    push    edi
    mov     ax, 2
    mov     bx, ax
    call    load_sectors
    pop     edi

    ; check minixv1 30 chars magic nbr, cleanly unmounted, logsize=0...
    mov     ax, WORD [edi + 0x10]
    cmp     ax, MINIX1_30CHARS
    jne     load_superblock_error
    mov     ax, WORD [edi + 0x12]
    cmp     ax, MINIX1_VALID
    jne     load_superblock_error
    mov     ax, WORD [edi + 0xa]
    cmp     ax, 0
    jne     load_superblock_error
    ret

    load_superblock_error:
    mov     edi, load_superblock_error_msg
    call    perror


; edi=itable dst, esi=superblock addr
; trash ax, bx, cx, edi
; returns nothing
load_itable:
    ; get offset of inode table in sectors from disk start in ax
    mov     ax, 2
    add     ax, WORD [esi + 0x4]
    add     ax, WORD [esi + 0x6]
    ; from blocks to sectors
    mov     bx, 2
    mul     bx
    mov     bx, ax

    ; get only a few sectors of itable; otherwise loading time is long as heck
    ; mov     ax, WORD [esi]
    ; mov     cx, 0x10
    ; div     cx
    mov     ax, 4
    call    load_sectors
    ret


; esi=address of dir, edi=address of itable, bx=addr of string, dx=nbr of sects
; trash ax, bx, dx, esi, edi
; returns address of file inode in edi
parse_directory:
    ; get number of inodes based on number of zones read
    mov     ax, 0x400 / 0x20
    mul     dx
    mov     dx, ax

    parse_directory_loop:
    add     esi, 0x2
    call    strcmp
    jz      parse_directory_found

    add     esi, 0x1e
    dec     dx
    jz      parse_directory_not_found
    jmp     parse_directory_loop

    parse_directory_found:
    movzx   eax, WORD [esi - 0x2]
    cmp     eax, 0x0
    je      parse_directory_not_found

    dec     eax
    mov     esi, 0x20
    mul     esi
    add     edi, eax
    ret

    parse_directory_not_found:
    mov     edi, parse_directory_not_found_error
    call    perror


; esi=inode addr, edi=file dst
; trash ax, bx, dx, esi, edi
; returns in dx the number of zones read
load_file:
    mov     dx, 0

    ; load first 7 direct zones
    mov     cx, 7
    add     esi, 0xe
    call    load_zones

    ; if the the direct zones were read and indirect zone pointer is not null
    mov     ax, WORD [esi]
    cmp     ax, 0
    je      load_file_ok

    ; read in memory the indirect zone
    push    edi
    mov     edi, indirect_zones_addr
    mov     bx, 2
    mul     bx
    mov     bx, ax
    mov     ax, 2
    call    load_sectors

    ; read the indirect zones list into memory
    pop     edi
    push    esi

    mov     cx, 512
    mov     esi, indirect_zones_addr
    call    load_zones

    pop     esi
    cmp     WORD [esi + 2], 0
    jne     load_file_error

    load_file_ok:
    ret

    load_file_error:
    mov     edi, load_file_error_msg
    call    perror


; esi=ptr to zone pointers, edi=zones dst, cx=nbr of zones, dx=zones acc thus
; far. trash ax, bx, cx, esi, edi
; returns updated esi and edi
load_zones:
    mov     ax, WORD [esi]
    cmp     ax, 0
    je      load_zones_ok

    mov     bx, 2
    mul     bx
    mov     bx, ax
    mov     ax, 2
    call    load_sectors

    ; increase
    add     esi, 0x2
    add     edi, 0x400
    inc     dx
    dec     cx
    jnz     load_zones

    load_zones_ok:
    ret


; edi=linked list dest
; trash eax, ebx, ecx, edi
; returns nothing
memprobe:
    xor     ebx, ebx

    ; setup the dword holding table size
    mov     esi, edi
    add     edi, 20
    mov     DWORD [esi], 0

    memprobe_probe:
    mov     edx, 0x534d4150
    mov     eax, 0xe820
    mov     ecx, 20
    int     0x15
    add     edi, 20

    jc      memprobe_error
    cmp     ah, 0
    je      memprobe_error

    add     DWORD [esi], 1
    cmp     ebx, 0
    jne     memprobe_probe

    memprobe_ok:
    ret

    memprobe_error:
    mov     edi, memprobe_error_msg
    call    perror


bootloader_end_warning:
    times   1024-($-$$) db 0x00
