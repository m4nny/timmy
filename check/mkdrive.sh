#!/bin/sh


DRIVE=$1
MOUNTPOINT=$2
ROOTFS=$3
BOOTCODE=$4

test -e ${BOOTCODE} || exit 1
test -e ${ROOTFS} || exit 2

# generate drive
rm -rf ${DRIVE}
dd if=/dev/zero of=${DRIVE} bs=512 count=$((64 * 1024))
mkfs.minix -1 ${DRIVE}
dd if=${BOOTCODE} of=${DRIVE} bs=512 count=2 conv=notrunc

# setup loop device with ok rights
test -z "$(lsmod | grep loop)" && sudo modprobe loop
LOOP=`sudo losetup --find --show -o0 ${DRIVE}`
test -b ${LOOP} || sudo mknod ${LOOP} b 7 0;

# mount the loop device
mkdir -p ${MOUNTPOINT};
sudo mount -t minix ${LOOP} ${MOUNTPOINT};

# copy rootfs dir content
sudo cp -Lr ${ROOTFS}/* ${MOUNTPOINT};

# unmount the loop device
sudo umount ${MOUNTPOINT};
rm -rf ${MOUNTPOINT};

# remove the loop device
sudo losetup --detach ${LOOP};
