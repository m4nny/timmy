all: drive

bootstrap:
	nasm -f bin boot/boot.nasm -o boot/boot.bin

kernel:
	mkdir -p check/rootfs/boot
	make all -C kernel
	cp kernel/kernel.bin check/rootfs/boot/kernel
.PHONY: kernel

drive: bootstrap kernel
	check/mkdrive.sh drive check/mnt check/rootfs boot/boot.bin

qemu: drive
	rm -rf check/qemu-logs
	qemu-system-i386 -hda drive -s -S &>check/qemu-logs\
		& sleep 1 && gdb $(GDBFLAGS) -x check/qemu-gdbinit

bochs: drive
	rm -rf check/bochs-logs
	echo 6 | bochs -q 'boot:disk' "ata0-master: type=disk, path=drive"\
		'gdbstub: enabled=1, port=1234, text_base=0, data_base=0, bss_base=0'\
		"romimage: file=/usr/share/bochs/BIOS-bochs-latest"\
		"vgaromimage: file=/usr/share/bochs/VGABIOS-lgpl-latest"\
		&>check/bochs-logs\
		& sleep 1 && gdb $(GDBFLAGS) -x check/bochs-gdbinit && killall bochs

clean:
	rm -f boot/boot.bin drive
	rm -f check/qemu-logs
	rm -f check/bochs-logs
	rm -rf check/rootfs/
	make clean -C kernel
