BITS 32
SECTION .text


extern drive_init
extern memory_init
extern screen_init
extern idt_init
extern irq_init
extern pit_init
extern kb_init
extern vfs_init
extern pci_init
extern main
extern memory_analysis
extern memory_heap_offset;
extern kernel_end
global entry
global halt


; the stack, as given by the bootcode, resembles:
;
;       +----------------+
;       |   memory_map   |
;       +----------------+ esp


entry:
    ; initialize framebuffer first for error/specs reporting
    ; as well as the vfs first (allows printing of messages)
    call    screen_init
    call    vfs_init

    call    memory_init
    mov     esp, eax

    call    drive_init
    call    idt_init
    call    irq_init
    call    pit_init
    call    kb_init
    call    pci_init

    entry_done:
    call    main
    jmp     halt


halt:
    hlt
    jmp     halt
