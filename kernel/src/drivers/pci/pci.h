#ifndef _DRIVERS_PCI_PCI_HEADER
# define _DRIVERS_PCI_PCI_HEADER

# include <linux/stdint.h>
# include <tools/system.h>
# include <tools/debug.h>


struct pci_bdf
{
    uint16_t bus;
    uint8_t dev;
    uint8_t fun;
    struct pci_bdf* next;
};


struct pci_ident
{
    uint8_t classcode;
    uint8_t subclasscode;
    uint8_t progif;
};


void pci_init(void);
struct pci_bdf* pci_get_bdf(struct pci_ident* id);
void* pci_get_bar(uint32_t bus, uint16_t dev, uint8_t fun, uint8_t* barid);

uint32_t pci_read(uint32_t bus, uint32_t dev, uint32_t func, uint32_t off);
void pci_write(uint32_t bus, uint32_t dev, uint32_t func, uint32_t off,
    uint32_t value);

#endif // !_DRIVERS_PCI_PCI_HEADER
