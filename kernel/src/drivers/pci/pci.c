#include <drivers/pci/pci.h>

#define PCI_CFG_ADDR    0xcf8
#define PCI_CFG_DATA    0xcfc
#define PCI_PROCESS_ADDRESS(enable, bus, device, func, off)                   \
(                                                                             \
    (uint32_t) ((enable & 0x1) << 31) |                                       \
    (uint32_t) ((bus & 0xff) << 16) |                                         \
    (uint32_t) ((device & 0x1f) << 11) |                                      \
    (uint32_t) ((func & 0x7) << 8) |                                          \
    (uint32_t) ((off & 0xfc) | 0)                                             \
)


uint32_t
pci_read(uint32_t bus, uint32_t dev, uint32_t func, uint32_t off)
{
    OUTL(PCI_CFG_ADDR, PCI_PROCESS_ADDRESS(1, bus, dev, func, off));
    return inl(PCI_CFG_DATA);
}


void
pci_write(uint32_t bus, uint32_t dev, uint32_t func, uint32_t off,
    uint32_t value)
{
    OUTL(PCI_CFG_ADDR, PCI_PROCESS_ADDRESS(1, bus, dev, func, off));
    OUTL(PCI_CFG_ADDR, value);
}


static uint16_t
pci_get_vendorid(uint32_t bus, uint16_t dev, uint8_t fun)
{
    return pci_read(bus, dev, fun, 0) & 0xffff;
}


static uint8_t
pci_get_headertype(uint32_t bus, uint16_t dev, uint8_t fun)
{
    return (pci_read(bus, dev, fun, 0xc) >> 16) & 0xff;
}


void*
pci_get_bar(uint32_t bus, uint16_t dev, uint8_t fun, uint8_t* barid)
{
    if ((pci_get_headertype(bus, dev, fun) & 0x7f) != 0x0)
        PANIC("%s\n", "only general pci devices are supported");

    // bartype is 0 if it's a I/O port , 0 if I/O memory
    uint32_t bar = pci_read(bus, dev, fun, 0x10 + 0x4 * *barid);
    *barid = (bar & 0x1);
    return (void*) (bar & (*barid ? 0xfffffffc : 0xfffffff0));
}


static struct pci_bdf*
pci_check_bdf(uint32_t bus, uint16_t dev, uint8_t fun, struct pci_ident* id)
{
    uint32_t tmp = pci_read(bus, dev, fun, 0x8);
    uint8_t cc = tmp >> 24;
    uint8_t subcc = (tmp >> 16) & 0xff;
    uint8_t pg = (tmp >> 8) & 0xff;

    if (cc != id->classcode || subcc != id->subclasscode || pg != id->progif)
        return (void*) 0x0;

    struct pci_bdf* bdf = malloc(sizeof (struct pci_bdf));
    bdf->bus = bus; bdf->dev = dev; bdf->fun = fun; bdf->next = 0x0;
    return bdf;
}


struct pci_bdf*
pci_get_bdf(struct pci_ident* id)
{
    struct pci_bdf* bdf = 0x0, *head = 0x0;

    for (uint32_t bus = 0; bus < 256; bus += 1)
    {
        for (uint16_t dev = 0; dev < 32; dev += 1)
        {
            // device does not exist
            if (pci_get_vendorid(bus, dev, 0) == 0xffff)
                continue;

            // multi functions or not?
            uint8_t funcount = (pci_get_headertype(bus, dev, 0) >> 7) * 7 + 1;
            for (uint8_t i = 0; i < funcount; i += 1)
            {
                struct pci_bdf* tmp = pci_check_bdf(bus, dev, i, id);

                if (tmp == 0x0)
                    continue;
                else if (bdf == 0x0)
                    bdf = head = tmp;
                else
                {
                    bdf->next = tmp;
                    bdf = bdf->next;
                }
            }
        }
    }

    return head;
}


void
pci_init(void)
{
    PRINT("%s\n", "pci initialization... done");
}
