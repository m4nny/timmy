#ifndef _DRIVERS_SCREEN_HEADER
# define _DRIVERS_SCREEN_HEADER

# include <linux/stdint.h>
# include <tools/system.h>
# include <fs/structs.h>


uint32_t screen_write(struct file* file, const char* src, uint32_t size);
uint32_t screen_read(struct file* file, void* dst, uint32_t size);
uint32_t screen_lseek(struct file* file, int32_t offset, uint8_t whence);
void screen_putstr(const char* str);
void screen_init(void);


#endif // !_DRIVERS_SCREEN_HEADER
