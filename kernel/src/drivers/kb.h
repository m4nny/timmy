#ifndef _DRIVERS_KB_HEADER
# define _DRIVERS_KB_HEADER

# include <linux/stdint.h>
# include <tools/debug.h>
# include <pic/irq.h>
# include <pic/idt.h>


void kb_init(void);
uint32_t kb_read(struct file* file, void* dst, uint32_t size);
uint32_t kb_write(struct file* file, const char* src, uint32_t size);
uint32_t kb_lseek(struct file* file, int32_t offset, uint8_t whence);


#endif // !_DRIVERS_KB_HEADER
