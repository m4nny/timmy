#ifndef _DRIVERS_DRIVE_HEADER
# define _DRIVERS_DRIVE_HEADER

# include <linux/stdint.h>
# include <fs/structs.h>
# include <tools/debug.h>


uint32_t drive_read(struct file* file, void* dst, uint32_t size);
uint32_t drive_write(struct file* file, const char* src, uint32_t size);
uint32_t drive_lseek(struct file* file, int32_t offset, uint8_t whence);
void drive_init(void);


#endif // !_DRIVERS_DRIVE_HEADER
