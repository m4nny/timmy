#include <drivers/drive.h>

#define DRIVE_READ  0x20
#define DRIVE_WRITE 0x30
#define SECTOR_SIZE 0x200


uint32_t
drive_read(struct file* file, void* dst, uint32_t size)
{
    return size;
}


uint32_t
drive_write(struct file* file, const char* src, uint32_t size)
{
    return size;
}


uint32_t
drive_lseek(struct file* file, int32_t offset, uint8_t whence)
{
    return (uint32_t) offset;
}


void
drive_init(void)
{
    PRINT("%s\n", "hdd drivers initialization... done");
}
