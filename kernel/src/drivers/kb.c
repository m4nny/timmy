#include <drivers/kb.h>

#define KB_ISR_INDEX    33
#define KB_DATA_PORT    0x60
#define KB_BUFFER_FULL  (inb(0x64) & 0x1)
#define KB_PRESSED(sc)  (!(sc & 0x80))


void isr_kb(void);
static uint32_t kb_queue_ptr = 0;
static uint32_t kb_queue[32];
static char kb_scanmap[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b',
    '\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n', 0,
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`',   0, '\\',
    'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 0, '*', 0, ' ', 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0, 0, 0, '+', 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0
};


static void
kb_enqueue(uint32_t scancode)
{
    if (kb_queue_ptr == sizeof (kb_queue))
        return;

    kb_queue[kb_queue_ptr++] = scancode;
}


static uint32_t
kb_dequeue(void)
{
    if (kb_queue_ptr == 0)
        return -1;

    uint32_t scancode = kb_queue[0];
    for (uint32_t i = 0; i < kb_queue_ptr - 1; ++i)
        kb_queue[i] = kb_queue[i + 1];

    --kb_queue_ptr;
    return scancode;
}


void
kb_interrupt(void)
{
    while (!KB_BUFFER_FULL)
        ;

    uint8_t scancode = inb(KB_DATA_PORT);
    if (KB_PRESSED(scancode))
        kb_enqueue(scancode);

    ack_last_interrupt();
}


uint32_t
kb_write(struct file* file, const char* src, uint32_t size)
{
    return 0;
}


uint32_t
kb_read(struct file* file, void* dst, uint32_t size)
{
    uint32_t scancode = -1;

    for (uint32_t i = 0; i < size; i += 1)
    {
        while ((scancode = kb_dequeue()) == -1)
            __asm__ volatile ("hlt");
        *((char*) dst + i) = kb_scanmap[scancode];
    }

    return size;
}


uint32_t
kb_lseek(struct file* file, int32_t offset, uint8_t whence)
{
    return 0;
}


void
kb_init(void)
{
    PRINT("%s ", "kb initialization...");
    idt_register(KB_ISR_INDEX, isr_kb);
    clear_interrupt(1);
    PRINT("%s\n", "done");
}
