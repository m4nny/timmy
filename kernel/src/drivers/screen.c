#include <drivers/screen.h>

#define COLS            80
#define ROWS            25
#define TABWIDTH        8
#define VGA_CRTC_INDEX  0x3d4
#define VGA_CRTC_DATA   0x3d5


static char* framebuffer = (void*) 0xb8000;
static uint8_t color = 0x7;
static struct {
    uint8_t row;
    uint8_t col;
} cursor;


static void
screen_putc(char c, uint8_t row, uint8_t col, uint8_t color)
{
    uint16_t pos = (row*COLS + col)*2;

    framebuffer[pos] = c;
    framebuffer[pos + 1] = color;
}


static void
screen_scroll(void)
{
    uint8_t row;
    uint8_t col;

    // shift all lines up by one row
    for (row = 0; row < ROWS - 1; row++)
    {
        for (col = 0; col < COLS; col++)
        {
            uint16_t pos = ((row + 1)*COLS + col)*2;
            screen_putc(framebuffer[pos], row, col, framebuffer[pos + 1]);
        }
    }

    // clear the last line
    for (col = 0; col < COLS; col++)
        screen_putc(0, ROWS - 1, col, 0);
}


static void
screen_cursor_update(void)
{
    uint16_t pos = (cursor.row*COLS) + cursor.col;

    OUTB(VGA_CRTC_INDEX, 0x0F);
    OUTB(VGA_CRTC_DATA, (uint8_t)(pos & 0xFF));

    OUTB(VGA_CRTC_INDEX, 0x0E);
    OUTB(VGA_CRTC_DATA, (uint8_t)((pos >> 8) & 0xFF));
}


static void
screen_cursor_set(uint8_t row, uint8_t col)
{
    cursor.row = row;
    cursor.col = col;

    screen_cursor_update();
}


static void
screen_cursor_newline(void)
{
    cursor.row += 1;
    cursor.col = 0;

    if ((cursor.row % ROWS) == 0) {
        cursor.row = ROWS - 1;
        screen_scroll();
    }
}


static void
screen_cursor_step(void)
{
    if (++cursor.col >= COLS)
        screen_cursor_newline();
}


static void
screen_clear(void)
{
    uint8_t row;
    uint8_t col;

    for (row = 0; row < ROWS; row++)
        for (col = 0; col < COLS; col++)
            screen_putc(0, row, col, color);

    screen_cursor_set(0, 0);
}


static void
screen_print(char c)
{
    switch (c)
    {
    case '\n':
        screen_cursor_newline();
        break;
    case '\r':
        screen_cursor_set(0, 0);
        break;
    case '\t':
        for (uint32_t i = 0; i < TABWIDTH; ++i)
        {
            screen_putc(c, cursor.row, cursor.col, color);
            screen_cursor_step();
        }
        break;
    default:
        screen_putc(c, cursor.row, cursor.col, color);
        screen_cursor_step();
        break;
    }

    screen_cursor_update();
}


void
screen_putstr(const char* str)
{
    while (*str != '\0')
        screen_print(*str++);
}


uint32_t
screen_write(struct file* file, const char* src, uint32_t size)
{
    uint32_t i;
    for (i = 0; i < size; ++i)
        screen_print(src[i]);
    return i;
}


uint32_t
screen_read(struct file* file, void* dst, uint32_t size)
{
    return 0;
}


uint32_t
screen_lseek(struct file* file, int32_t offset, uint8_t whence)
{
    int8_t off = (int8_t) offset;
    uint8_t row = cursor.row;
    uint8_t col = cursor.col;

    if (whence == SEEK_CUR)
    {
        if (off > (int8_t) col)
        {
            // TODO: non-trivial SEEK_CUR
        }
        else
            col = (uint8_t) ((int8_t) col + off);
    }

    // TODO: SEEK_SET

    screen_cursor_set(row, col);
    return cursor.row * COLS + cursor.col;
}


void
screen_init(void)
{
    screen_clear();
    screen_putstr("screen drivers initialization... done\n");
}
