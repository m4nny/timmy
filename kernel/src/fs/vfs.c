#include <fs/vfs.h>


static struct fs* filesystems = (void*) 0x0;


void
vfs_init(void)
{
    screen_putstr("vfs initialization... ");

    filesystems = fs_minix_init();
    struct fs* fs = filesystems;

    fs->next = fs_sys_init();
    fs = fs->next;
    fs->next = (void*) 0x0;

    screen_putstr("done\n");
}


struct file*
open(const char* filename, uint32_t flags)
{
    const char* subfilename = filename;
    char* fname = strdup(filename);
    char** splitted = strsplit(fname, '/');
    f_open func = (void*) 0x0;

    for (struct fs* ptr = filesystems; ptr != (void*) 0x0; ptr = ptr->next)
        if (strcmp(ptr->name, splitted[0]) == 0)
            func = ptr->openfunc;
    if (func == (void*) 0x0)
        PANIC("%s: %s\n", filename, "file not found");

    subfilename += strlen(splitted[0]) + 1;
    free(splitted);
    free(fname);
    return func(subfilename, flags);
}


uint32_t
write(struct file* file, const char* str, uint32_t size)
{
    return file->writefunc(file, str, size);
}


uint32_t
read(struct file* file, void* str, uint32_t size)
{
    return file->readfunc(file, str, size);
}


uint32_t
lseek(struct file* file, int32_t offset, uint8_t whence)
{
    return file->lseekfunc(file, offset, whence);
}


uint32_t
close(struct file* file)
{
    return file->container->closefunc(file);
}
