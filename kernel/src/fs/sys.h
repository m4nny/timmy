#ifndef _FS_SYS_HEADER
# define _FS_SYS_HEADER

# include <linux/stdint.h>
# include <tools/string.h>
# include <tools/debug.h>

# include <drivers/screen.h>
# include <drivers/drive.h>
# include <drivers/kb.h>
# include <fs/structs.h>


struct fs* fs_sys_init(void);


#endif // !_FS_SYS_HEADER
