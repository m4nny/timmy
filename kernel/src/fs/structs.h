#ifndef _FS_STRUCTS_HEADER
# define _FS_STRUCTS_HEADER

# define SEEK_SET 0x1
# define SEEK_CUR 0x2
# define SEEK_END 0x4


typedef struct file* (*f_open)(const char*, uint32_t);
typedef uint32_t (*f_write)(struct file*, const char*, uint32_t);
typedef uint32_t (*f_read)(struct file*, void*, uint32_t);
typedef uint32_t (*f_lseek)(struct file*, int32_t, uint8_t);
typedef uint32_t (*f_close)(struct file*);


struct fs
{
    const char* name;

    f_open openfunc;
    f_close closefunc;

    struct fs* next;
};


struct file
{
    const char* name;
    struct fs* container;

    f_write writefunc;
    f_read readfunc;
    f_lseek lseekfunc;
}; typedef struct file* file_t;


#endif // !_FS_STRUCTS_HEADER
