#ifndef _FS_MINIX_HEADER
# define _FS_MINIX_HEADER

# include <linux/stdint.h>
# include <tools/memory.h>
# include <tools/string.h>
# include <fs/structs.h>


struct fs* fs_minix_init(void);


#endif // !_FS_MINIX_HEADER
