#include <fs/minix.h>

static struct file* fs_minix_open(const char* filename, uint32_t flags);
static uint32_t fs_minix_close(struct file* file);
static struct fs fs = { "minix", fs_minix_open, fs_minix_close, 0x0 };


static struct file*
fs_minix_open(const char* filename, uint32_t flags)
{
    return (void*) 0x0;
}


static uint32_t
fs_minix_close(struct file* file)
{
    return 0;
}


struct fs*
fs_minix_init(void)
{
    return &fs;
}
