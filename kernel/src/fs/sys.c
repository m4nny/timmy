#include <fs/sys.h>

static struct file* fs_sys_open(const char* filename, uint32_t flags);
static uint32_t fs_sys_close(struct file* file);
static struct fs fs = { "sys", fs_sys_open, fs_sys_close, 0x0 };
static struct file devices[] =
{
    { "screen", &fs, screen_write, screen_read, screen_lseek },
    { "kb", &fs, kb_write, kb_read, kb_lseek },
    { "drive", &fs, drive_write, drive_read, drive_lseek },
};


static struct file*
fs_sys_open(const char* filename, uint32_t flags)
{
    for (uint32_t i = 0; i < sizeof (devices); i += 1)
        if (strcmp(devices[i].name, filename + 1) == 0)
            return devices + i;
    PANIC("%s: %s\n", filename, "file not found");
    return (void*) 0x0;
}


static uint32_t
fs_sys_close(struct file* file)
{
    return 0;
}


struct fs*
fs_sys_init(void)
{
    return &fs;
}
