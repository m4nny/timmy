#ifndef _VFS_HEADER
# define _VFS_HEADER

# include <linux/stdint.h>
# include <drivers/screen.h>
# include <tools/string.h>
# include <tools/debug.h>
# include <fs/structs.h>
# include <fs/minix.h>
# include <fs/sys.h>


void vfs_init(void);
struct file* open(const char* filename, uint32_t flags);
uint32_t write(struct file* file, const char* str, uint32_t size);
uint32_t read(struct file* file, void* str, uint32_t size);
uint32_t lseek(struct file*, int32_t offset, uint8_t);
uint32_t close(struct file* file);


#endif // !_VFS_HEADER
