#ifndef _STRING_HEADER
# define _STRING_HEADER

# include <linux/stdint.h>
# include <tools/memory.h>


void* memcpy(void* dst, const void* src, uint32_t n);
void* memset(void *s, uint8_t c, uint32_t n);
uint32_t strlen(const char* s);
uint32_t strcmp(const char* s1, const char* s2);
char* strdup(const char* s);
char** strsplit(char* begin, const char sep);
char* itoa(uint32_t n, uint8_t base);
uint32_t atoi(const char* n);


#endif // !_STRING_HEADER
