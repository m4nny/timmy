#ifndef _TOOLS_PRINTF_HEADER
# define _TOOLS_PRINTF_HEADER

# include <linux/stdint.h>
# include <linux/stdarg.h>
# include <tools/string.h>
# include <fs/vfs.h>

# define META '%'


union types
{
    char chr;
    char *str;
    uint32_t integer;
};


uint32_t sprintf(struct file* file, const char *format, ...);


#endif // !_TOOLS_PRINTF_HEADER
