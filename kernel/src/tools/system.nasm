BITS 32
SECTION .text


global outl
global inl
global outb
global inb


outl:
    mov     eax, DWORD [esp + 0x4]
    mov     dx, WORD [esp + 0x8]
    out     dx, eax
    ret


inl:
    mov     dx, WORD [esp + 0x4]
    in      eax, dx
    ret


outb:
    mov     eax, DWORD [esp + 0x4]
    mov     dx, WORD [esp + 0x8]
    out     dx, al
    ret


inb:
    xor     eax, eax
    mov     dx, WORD [esp + 0x4]
    in      al, dx
    ret
