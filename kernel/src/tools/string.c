#include <tools/string.h>


void*
memcpy(void* dst, const void* src, uint32_t n)
{
    for (uint32_t i = 0; i < n; i += 1)
        ((uint8_t*) dst)[i] = ((const uint8_t*) src)[i];
    return dst;
}


void*
memset(void *s, uint8_t c, uint32_t n)
{
    for (uint32_t i = 0; i < n; i += 1)
        ((uint8_t*) s)[i] = c;
    return s;
}


uint32_t
strlen(const char* s)
{
    uint32_t len = 0;
    while (*s++ != '\0')
        len += 1;
    return len;
}


uint32_t
strcmp(const char* s1, const char* s2)
{
    while ((*s1++ == *s2++) && (*s1 != '\0'));
    return s1[-1] - s2[-1];
}


char*
strdup(const char* s)
{
    uint32_t len = strlen(s);
    char* ret = malloc(len + 1);

    for (uint32_t i = 0; i < len + 1; i += 1)
        ret[i] = s[i];

    return ret;
}


char**
strsplit(char* begin, const char sep)
{
    // 24 is the maximum directories depth handled
    char** output = calloc(24, sizeof (char *));
    char* tmp = begin;
    uint32_t ptr = 0;

    for (uint32_t i = 0; tmp[i] != '\0'; i += 1)
    {
        if (tmp[i] != sep)
            continue;
        // do not take the first chunk (src str starts with sep
        // and ignore the case where two separators follow each others
        else if (i != 0 && *begin != sep)
            output[ptr++] = begin;

        tmp[i] = '\0';
        begin = tmp + i + 1;
    }

    // except when ending with a separator, do not forget the last chunk
    if (*begin != '\0')
        output[ptr] = begin;
    return output;
}


char*
itoa(uint32_t n, uint8_t base)
{
    char* buff = malloc(20  * sizeof (char));
    uint8_t i = 0;

    if (base == 16)
        buff[i++] = 'h';
    if (n == 0)
        buff[i++] = '0';

    for (; n != 0; i += 1, n /= base)
        buff[i] = "0123456789abcdef"[(n % base)];
    if (base == 8)
        buff[i++] = '0';
    buff[i--] = '\0';

    for (uint8_t j = 0; j < i; j += 1, i -= 1)
    {
        buff[i] ^= buff[j];
        buff[j] ^= buff[i];
        buff[i] ^= buff[j];
    }

    return buff;
}


uint32_t
atoi(const char* n)
{
    uint32_t result = 0;
    uint8_t base = base = (n[0] == '0' ? (n[1] == 'x' ? 16 : 8) : 10);
    for (const char* v = n + (base == 16 ? 2 : (base == 8 ? 1 : 0)); *v; v++)
        result = (result * base) + (*v - '0');
    return result;
}
