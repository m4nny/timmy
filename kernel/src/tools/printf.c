#include <tools/printf.h>


static uint32_t
print_integer(struct file* file, uint32_t input, uint8_t base)
{
    char* result = itoa(input, base);
    uint32_t len = strlen(result);
    write(file, result, len);
    free(result);
    return len;
}


static uint32_t
print_string(struct file* file, char* input)
{
    uint32_t len = 0;

    if (input == (void*) 0x0)
        len = write(file, "(nil)", 6);
    else
    {
        len = strlen(input);
        write(file, input, len);
    }

    return len;
}


uint32_t
sprintf(struct file* file, const char *format, ...)
{
    union types arg;
    va_list ap;
    uint32_t len = 0;

    va_start(ap, format);

    for (uint8_t i = 0; format[i] != '\0'; i += (format[i] == META ? 2 : 1))
    {
        if (format[i] == META && format[i + 1] != META)
            arg = va_arg(ap, union types);

        if (format[i] != META)
            len += write(file, format + i, 1);
        else if (format[i + 1] == 'x' || format[i + 1] == 'p')
            len += print_integer(file, arg.integer, 16);
        else if (format[i + 1] == 'd')
            len += print_integer(file, arg.integer, 10);
        else if (format[i + 1] == 'o')
            len += print_integer(file, arg.integer, 8);
        else if (format[i + 1] == 's')
            len += print_string(file, arg.str);
        else if (format[i + 1] == 'c')
            len += write(file, &arg.chr, 1);
    }

    va_end(ap);
    return len;
}
