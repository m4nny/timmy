#ifndef _DEBUG_HEADER
# define _DEBUG_HEADER

# include <drivers/screen.h>
# include <tools/printf.h>
# include <fs/vfs.h>

# define PRINT(msg, ...)\
{\
    file_t __scr = open("/sys/screen", 0);\
    sprintf(__scr, msg, __VA_ARGS__);\
    close(__scr);\
}

# define PRINT_NEWLINE { PRINT("%s", "\n"); }
# define DEBUG(msg, ...) { PRINT("DEBUG: " msg, __VA_ARGS__); }
# define ERROR(msg, ...) { PRINT("ERROR: " msg, __VA_ARGS__); }
# define PANIC(msg, ...) { PRINT("PANIC: " msg, __VA_ARGS__); while (1); }

#endif // !_DEBUG_HEADER
