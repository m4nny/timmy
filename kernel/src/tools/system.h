#ifndef _TOOLS_SYSTEM_HEADER
# define _TOOLS_SYSTEM_HEADER

# include <linux/stdint.h>

# define OUTB(Port, Value)      outb(Value, Port);
# define INB(Port, Value)       Value = inb(Port);
# define OUTL(Port, Value)      outl(Value, Port);
# define INL(Port, Value)       Value = inl(Port);


uint32_t inl(uint16_t port);
void outl(uint32_t value, uint16_t port);

uint8_t inb(uint16_t port);
void outb(uint8_t value, uint16_t port);


#endif // !_TOOLS_SYSTEM_HEADER
