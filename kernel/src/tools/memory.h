#ifndef _MEMORY_HEADER
# define _MEMORY_HEADER

# include <linux/stdint.h>
# include <tools/debug.h>


struct memory_region
{
    uint64_t base;
    uint64_t size;
    uint32_t type;
} __attribute__((packed));


struct memory_chunk
{
    uint32_t base;
    uint32_t size;
    struct memory_chunk* next;
} __attribute__((packed));


void* malloc(uint32_t size);
void* calloc(uint32_t elts_count, uint32_t elt_size);
void free(void* ptr);
uint32_t memory_init(struct memory_region* memory_map);
void* amalloc(uint32_t size, uint32_t boundary);


#endif // !_MEMORY_HEADER
