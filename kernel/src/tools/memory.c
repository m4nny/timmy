#include <tools/memory.h>

#define AVAILABLE   0x1
#define RESERVED    0x2
#define NODES_MAX   64
#define NODES_SIZE  (NODES_MAX * sizeof (struct memory_chunk))


void* memory_heap_offset = (void*) 0x0;
static struct memory_chunk* chunks = (void*) 0x0;
static struct memory_chunk* freenodes = (void*) 0x0;
static struct memory_chunk* tail = (void*) 0x0;
void* memcpy(void* dst, const void* src, uint32_t n);
void* memset(void *s, uint8_t c, uint32_t n);
char* itoa(uint32_t n, uint8_t base);
extern void* kernel_end;


static struct memory_chunk*
memory_fill_new_nodes(void* addr)
{
    struct memory_chunk* ptr = addr;
    for (uint32_t i = 0; i < NODES_MAX; i += 1)
        ptr[i].next = ptr + i + 1;
    ptr[NODES_MAX - 1].next = 0x0;
    return addr;
}


static struct memory_chunk* memory_get_node(uint8_t pick_last);
void*
memory_malloc(uint32_t size, uint8_t flags)
{
    struct memory_chunk* hold = chunks, *father = (void*) 0x0;
    while (hold != 0x0 && ((hold->base & 0x1) || hold->size < size))
    {
        father = hold;
        hold = (void*) hold->next;
    }

    // perfect match, do not need new node
    if (hold->size == size) // TODO?
    {
        hold->base |= 0x1;
        return hold;
    }

    // create new chunk
    struct memory_chunk* new = memory_get_node(flags);
    new->base = hold->base;
    new->size = size;
    new->next = hold;

    // update old chunk
    hold->base = new->base + new->size;
    hold->size = hold->size - new->size;
    new->base |= 0x1;

    // setup father in list
    if (father != 0x0)
        father->next = new;
    else
        chunks = new;
    // if hold is the "free space" chunk, then we save the tail
    // in case we need to allocate memory-aligned blocks in the future
    if (hold->next == (void*) 0x0)
        tail = new;

    return (void*) (new->base & 0xfffffffe);
}


static struct memory_chunk*
memory_get_node(uint8_t pick_last)
{
    struct memory_chunk* ret = freenodes;

    if (!pick_last && freenodes->next == 0x0)
        freenodes = memory_fill_new_nodes(memory_malloc(NODES_SIZE, 1));
    else
        freenodes = (void*) freenodes->next;

    return ret;
}


static void
memory_store_node(struct memory_chunk* node)
{
    if (freenodes != (void*) 0x0)
    {
        node->next = freenodes;
        freenodes = node;
    }
    else
    {
        node->next = 0x0;
        freenodes = node;
    }
}


static struct memory_chunk*
memory_merge_chunks(struct memory_chunk* father, struct memory_chunk* son)
{
    father->size += son->size;
    father->next = son->next;
    memory_store_node(son);
    return father;
}


void*
malloc(uint32_t size)
{
    // size is aligned with 4 bytes superior
    return memory_malloc((size + 3) & 0xfffffffc, 0);
}


void*
amalloc(uint32_t size, uint32_t boundary)
{
    uint32_t mask = 0xffffffff - boundary + 1;

    // insert the new block into the chunks linked list
    struct memory_chunk* freespace = tail->next;
    struct memory_chunk* new = memory_get_node(0);
    new->next = freespace;
    tail->next = new;

    if (freespace->base & 0x1)
        PANIC("%s: %s\n", "heap", "no more memory available");

    new->base = freespace->base & mask;
    if (new->base != freespace->base)
    {
        new->base += boundary;
        tail->next = memory_get_node(0);
        tail->next->next = new;
        tail->next->base = freespace->base;
        tail->next->size = new->base - freespace->base;
    }

    tail = new;
    new->size = size;
    freespace->size -= (new->base - freespace->base) + size;
    freespace->base = new->base + size;
    new->base |= 0x1;

    return (void*) (new->base & 0xfffffffe);
}


void*
calloc(uint32_t elts_count, uint32_t elt_size)
{
    uint32_t size = elt_size * elts_count;
    uint8_t* ret = malloc(size);
    memset(ret, 0, size);
    return (void*) ret;
}


void
free(void* ptr)
{
    struct memory_chunk* father = (void*) 0x0, *grandfather = (void*) 0x0;

    for (struct memory_chunk* idx = chunks; idx != 0x0;
        grandfather = father, father = idx, idx = idx->next)
    {
        if ((idx->base & 0xfffffffe) != (uint32_t) ptr)
            continue;

        idx->base &= 0xfffffffe;

        // update the tail pointer
        if (idx->next != (void*) 0x0 && idx->next->next == (void*) 0x0)
            tail = (father->base & 0x1) == 0 ? grandfather : father;

        if ((father->base & 0x1) == 0)
            idx = memory_merge_chunks(father, idx);
        if (idx->next != 0x0 && (idx->next->base & 0x1) == 0)
            memory_merge_chunks(idx, idx->next);
        break;
    }
}


static uint32_t
memory_analysis(struct memory_region* memory_map)
{
    // the first element is the size of the memory map as a dword
    uint16_t memory_map_size = *(uint16_t*) memory_map;
    memory_map++;

    // take the biggest chunk of memory,
    // setup the stack at the top and the heap at the bottom

    struct memory_region max = { .size = 0 };
    for (uint16_t i = 0; i < memory_map_size; i += 1)
    {
        if (memory_map[i].type != AVAILABLE)
            continue;

        if (memory_map[i].size > max.size)
            memcpy(&max, memory_map + i, sizeof (struct memory_region));
    }

    // set heap and stack
    memory_heap_offset = (void*) (uint32_t) (max.base & 0xffffffff);
    return (max.base + max.size) & 0xfffffff0;
}


uint32_t
memory_init(struct memory_region* memory_map)
{
    // retrieve the stack addr and initialize the stack heap
    uint32_t stackaddr = memory_analysis(memory_map);
    if (memory_heap_offset < (void*) &kernel_end &&
        (void*) &kernel_end < (void*) stackaddr)
        memory_heap_offset = (void*) &kernel_end;

    // align the memory_heap_offset with a 4 bytes boundary, just in case
    memory_heap_offset = (void*) ((uint32_t) memory_heap_offset & 0xfffffffc);

    freenodes = memory_fill_new_nodes(memory_heap_offset);
    memory_heap_offset = (void*) ((uint32_t) memory_heap_offset + NODES_SIZE);

    chunks = freenodes;
    chunks->base = (uint32_t) freenodes | 0x1;
    chunks->size = NODES_SIZE;
    chunks->next->base = (uint32_t) memory_heap_offset & 0xfffffffc;
    chunks->next->size = (stackaddr - (uint32_t) memory_heap_offset);

    freenodes = freenodes->next->next;
    chunks->next->next = (void*) 0x0;

    // print the amount of memory found
    PRINT("%s %d%s\n", "memory initialization...",
        (stackaddr - (uint32_t) memory_heap_offset) / 0x100000, "MB found");

    // set the stack in the asm entry routine
    return stackaddr;
}
