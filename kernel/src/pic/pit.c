#include <pic/pit.h>

#define PIT_IDT_INDEX   32
#define PIT_CMD         0x36
#define PIT_CTRL_REG    0x43
#define PIT_COUNTER0    0x40
#define INTERNAL_FREQ   (3579545 / 3)
#define SLEEP_FREQ      1000


f_pit pit_toexec = ack_last_interrupt;
static uint16_t freq = 1;
uint64_t sleep_counter = 0;
void isr_pit(void);


static void
sleep_stub(void)
{
    __asm__ volatile ("cli");
    sleep_counter -= 1;
    ack_last_interrupt();
    __asm__ volatile ("sti");
}


void
nanosleep(uint64_t milliseconds)
{
    f_pit save_func = pit_toexec;
    uint16_t save_freq = freq;

    sleep_counter = milliseconds;
    pit_set(sleep_stub, SLEEP_FREQ);
    while (sleep_counter != 0)
        __asm__ volatile ("hlt");
    pit_set(save_func, save_freq);
}


void
sleep(uint32_t seconds)
{
    uint64_t milliseconds = seconds * 1000;
    nanosleep(milliseconds);
}


void
pit_set(f_pit func, uint16_t freq)
{
    if (func != (void*) 0x0)
        pit_toexec = func;

    uint16_t divider = INTERNAL_FREQ / freq;
    OUTB(PIT_CTRL_REG, PIT_CMD);
    OUTB(PIT_COUNTER0, divider & 0xff);
    OUTB(PIT_COUNTER0, divider >> 8);
}


void
pit_init(void)
{
    PRINT("%s ", "pit initialization... ");

    // write the divider amount in the counter0 register
    // the default frequency is the one used for sleeping
    pit_set(ack_last_interrupt, SLEEP_FREQ);

    // setup the interrupt matching the pit
    idt_register(PIT_IDT_INDEX, isr_pit);
    clear_interrupt(0);

    PRINT("%s\n", "done");
}
