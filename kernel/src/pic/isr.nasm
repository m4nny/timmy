BITS 32
SECTION .text


global idt_flush


idt_flush:
    push    DWORD ebp
    mov     ebp, esp

    mov     eax, DWORD [ebp + 8]
    lidt    [eax]

    mov     esp, ebp
    pop     ebp
    ret


global isr_dummy
extern idt_unhandled
isr_dummy:
    cli
    pushad
    call    idt_unhandled
    popad
    iret


global isr_kb
extern kb_interrupt
isr_kb:
    cli
    pushad
    call    kb_interrupt
    popad
    iret


global isr_pit
extern pit_toexec
isr_pit:
    cli
    pushad
    mov     eax, DWORD [pit_toexec]
    cmp     eax, 0
    je      isr_pit_done
    call    eax
    isr_pit_done:
    popad
    iret
