#ifndef _PIC_IDT_HEADER
# define _PIC_IDT_HEADER

# include <linux/stdint.h>
# include <tools/memory.h>
# include <tools/debug.h>


struct idt_ptr
{
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));


void idt_register(uint8_t index, void* func);
void idt_init(void);


#endif // !_PIC_IDT_HEADER
