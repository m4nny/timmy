#ifndef _PIC_IRQ_HEADER
# define _PIC_IRQ_HEADER

# include <linux/stdint.h>
# include <tools/system.h>


void irq_init(void);
void clear_interrupt(uint8_t irqline);
void set_interrupt(uint8_t irqline);
void ack_last_interrupt(void);


#endif // !_PIC_IRQ_HEADER
