#ifndef _PIC_PIT_HEADER
# define _PIC_PIT_HEADER

# include <linux/stdint.h>
# include <pic/idt.h>
# include <pic/irq.h>


typedef void (*f_pit)(void);


void pit_set(f_pit func, uint16_t freq);
void pit_init(void);

void sleep(uint32_t seconds);
void nanosleep(uint64_t milliseconds);


#endif // !_PIC_PIT_HEADER
