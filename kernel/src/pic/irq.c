#include <pic/irq.h>

#define PIC_MASTER_CMD  0x20
#define PIC_MASTER_DATA 0x21

#define PIC_SLAVE_CMD   0xA0
#define PIC_SLAVE_DATA  0xA1


void
irq_init(void)
{
    // send icw1, enable icw4, cascade mode, level triggered
    OUTB(PIC_MASTER_CMD, 0x11);
    OUTB(PIC_SLAVE_CMD, 0x11);

    // send icw2, map irqs on the idt at offsets 32 and 40
    OUTB(PIC_MASTER_DATA, 0x20);
    OUTB(PIC_SLAVE_DATA, 0x28);

    // send icw3, connect slave on pin 4 of master
    OUTB(PIC_MASTER_DATA, 0x4);
    OUTB(PIC_SLAVE_DATA, 0x2);

    // send icw4, no special configuration atm
    OUTB(PIC_MASTER_DATA, 0x1);
    OUTB(PIC_SLAVE_DATA, 0x1);

    // mask all irqs
    OUTB(PIC_MASTER_DATA, 0xff);
    OUTB(PIC_SLAVE_DATA, 0xff);

    // enable hardware interrupts
    __asm__ volatile("sti");
}


void
clear_interrupt(uint8_t irqline)
{
    uint8_t value;
    uint16_t port;

    if (irqline < 8)
        port = PIC_MASTER_DATA;
    else
        port = PIC_SLAVE_DATA;

    // retrieve the current mask value
    INB(port, value);

    // process the new mask
    value &= ~(1 << irqline);

    // send the new mask
    OUTB(port, value);
}


void
set_interrupt(uint8_t irqline)
{
    uint8_t value;
    uint16_t port;

    if (irqline < 8)
        port = PIC_MASTER_DATA;
    else
        port = PIC_SLAVE_DATA;

    // retrieve the current mask value
    INB(port, value);

    // process the new mask
    value |= (1 << irqline);

    // send the new mask
    OUTB(port, value);
}


void
ack_last_interrupt(void)
{
    OUTB(PIC_MASTER_CMD, 0x20);
    OUTB(PIC_SLAVE_CMD, 0x20);
}
