#include <pic/idt.h>

#define COMPUTE_IDT_ENTRY(base, sele, flag)                                   \
(                                                                             \
    (uint64_t)(base & 0xffff0000) << 32 | (uint64_t)(base & 0x0000ffff) |     \
    (uint64_t)(sele & 0x0000ffff) << 16 | (uint64_t)(flag & 0x000000ff) << 40 \
)


static uint64_t* idt = (void*) 0x0;
void idt_flush(struct idt_ptr* ptr);
void ack_last_interrupt(void);
void isr_dummy(void);


void
idt_register(uint8_t index, void* func)
{
    idt[index] = COMPUTE_IDT_ENTRY((uint32_t) func, 0x10, 0x8f);
}


void
idt_unhandled(void)
{
    ERROR("%s\n", "unhandled exception");
    ack_last_interrupt();
}


void
idt_init(void)
{
    PRINT("%s ", "interrupts initialization...");

    // setup basic IDT with dummy ISR
    idt = malloc(256 * sizeof (uint64_t));
    for (uint16_t i = 0; i < 256; i += 1)
        idt_register(i, isr_dummy);

    struct idt_ptr ptr = { 256 * sizeof (uint64_t) - 1, (uint32_t) idt };
    idt_flush(&ptr);

    PRINT("%s\n", "done");
}
