#include <fs/vfs.h>
#include <tools/printf.h>
#include <pic/pit.h>


int main(void)
{
    nanosleep(500);

    file_t fb = open("/sys/screen", 0);
    file_t kb = open("/sys/kb", 0);
    char* buffer = calloc(256, sizeof (char));

    while (1)
    {
        sprintf(fb, "%s: ", "hit me");

        for (int32_t i = 0; i < 256; i += 1)
        {
            read(kb, buffer + i, 1);

            if (buffer[i] == '\b' && i > 0)
            {
                lseek(fb, -1, SEEK_CUR);
                write(fb, " ", 1);
                lseek(fb, -1, SEEK_CUR);
                i = (i - 2 < -1 ? -1 : i - 2);
            }
            else if (buffer[i] == '\n')
            {
                buffer[i] = '\0';
                break;
            }
            else
                sprintf(fb, "%c", buffer[i]);
        }

        sprintf(fb, "\nat %s: %x\n", buffer, *(uint32_t*) atoi(buffer));
    }

    free(buffer);
    close(kb);
    close(fb);

    while (1);
}
